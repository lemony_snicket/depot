Rails.application.routes.draw do
  devise_for :users
  # Redirects /orders/pending to /orders?pending=true.
  get '/orders/pending', to: redirect('orders?pending=true')

  resources :orders do
    get 'recent', on: :collection
    put 'ship', on: :member
  end
  
  resources :line_items do
    #put 'decrement', on: :member 
    put 'decrement', on: :member
  end
  resources :carts
  get 'store/index'

  resources :products do
    get :who_bought, on: :member
  end
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'store#index', as: 'store'
end
