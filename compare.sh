#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: compare.sh
# 
#         USAGE: ./compare.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 09/16/2016 14:49
#      REVISION:  2016-09-16 17:03
#===============================================================================

DIR=depot_k
PATH=/Users/rahul/Downloads/code/rails50/$DIR
FILE=$1
echo ${PATH}/$FILE    $1

/usr/local/bin/diff  -bw ${PATH}/$FILE $1

