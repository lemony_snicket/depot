class AddColumnsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :varchar
    add_column :users, :address, :text
    add_column :users, :phone, :varchar
  end
end
