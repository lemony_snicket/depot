class CopyPriceToLineItem < ActiveRecord::Migration[5.0]
  def up
    LineItem.all.each do |item|
      price = item.product.price
      item.price = price
      item.save!
    end
  end
  def down
    LineItem.all.each do |item|
      item.price = 0
      item.save!
    end
  end
end
