class UsersController < ApplicationController

  # display a list of users paginated
  def index
    @users = User.all.order(:created_at).paginate( page: params[:page] )
  end
  def edit
    @user = User.find(params[:id])
  end

  def update
    #@user = User.new(user_params)
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # handle a successful update
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  # profile page of user. user_path or link_to current_user
  def show
    @user = User.find(params[:id])
    # prevent viewing of user that is not activated
    #redirect_to root_url and return unless @user.activated?
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :address)
    end
end
